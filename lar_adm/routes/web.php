<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('main');
})->name ('main');

Route::get('/new_message', function () {
    return view('new_message');
})->name ('new_message');
Route::post('/contact/submit','control@submit'
)->name ('contact-form');
Route::get('/contact/all','control@allData'
)->name ('allData');



Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
