@extends('layout.my_app')

@section('main_title')
Mess_title
@endsection



@section('content')
<h1>Messages</h1>
 @foreach($data as $el)
  <div class="alert alert-info" >
    <img src="{{asset( 'storage/' .$el->image) }}" width="850" height="250"/>
 <h3>{{$el->name}}</h3>
<h5>{{$el->subject}}</h5>
<p>{{$el->message}}</p>

<p><small>{{$el->created_at}}</small></p>
</div>
 @endforeach
@endsection

