<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <title>@yield('main_title')</title>
</head>
<body>
@include('inc.header')
{{-- @if(Request::is('/'))
@include('inc.hero')
@endif --}}

<div class="container mt-5" >
@include('inc.messages')
@yield('content')
</div>

</body>
</html>
