@extends('layout.my_app')

@section('main_title')
Contact
@endsection

@section('content')
<h1>contact</h1>



<form action="{{route('contact-form')}}" method="post" enctype="multipart/form-data">
@csrf
<div class="form-group">
    <label for="image">image</label>
<input name="image" type="file">
</div>
<div class="form-group">
<label for="name">Name</label>
<input type="text"name="name" class="form-control">
</div>
<div class="form-group">

<label for="subject">subject</label>
<input type="text"name="subject" class="form-control">
</div>
<div class="form-group">
<label for="message">text</label>
<textarea name="message" id="message" cols="80" rows="6"></textarea>
</div>

<button type="submit" class="btn btn-success">Go</button>
</form>

 @endsection
