<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use lar_news\storage\img\j;
class CreateNewsModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('model_news', function (Blueprint $table) {
            $table->id();
            $table->string('image')->nullable(); // our profile image field
            $table->string('name');
            $table->string('subject');
            $table->text('message');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('model_news');
    }
}
